from enum import Enum


class Registers(Enum):
    MODE1 = 0x00
    PRE_SCALE = 0xfe


class RegisterMode1(Enum):
    RESTART = 0b10000000
    EXTCLK = 0b01000000
    AI = 0b00100000
    SLEEP = 0b00010000
    SUB1 = 0b00001000
    SUB2 = 0b00000100
    SUB3 = 0b00000010
    ALLCALL = 0b00000001
