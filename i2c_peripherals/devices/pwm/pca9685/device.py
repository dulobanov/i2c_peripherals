import logging
from fractions import Fraction
from struct import pack

from i2c_peripherals.mixins.i2c_mixin import I2CMixin
from i2c_peripherals.base.base_peripheral import BasePeripheral
from i2c_peripherals.devices.pwm.pca9685.registers import RegisterMode1, Registers

logger = logging.getLogger(__name__)


class PCA9685(I2CMixin, BasePeripheral):
    MIN_FREQUENCY = 24
    MAX_FREQUENCY = 1526
    TICKS_PER_CYCLE = 4095

    SECOND = 1000000

    OSC_CLOCK_DEFAULT = 25000000

    ALL_PORTS_START_REGISTER = 0xfa

    PORT_START_REGISTER = 0x06
    PORT_REGISTERS_AMOUNT = 4

    def __init__(self, frequency, osc_clock=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._osc_clock = osc_clock or self.OSC_CLOCK_DEFAULT
        assert self.MIN_FREQUENCY <= frequency <= self.MAX_FREQUENCY, "Unsupported frequency"
        self._frequency = frequency
        self._ticks_per_us = Fraction(
            self.TICKS_PER_CYCLE * self._frequency,
            self.SECOND,
        )
        self._period_time = self.SECOND / self._frequency

        pass

    def _start(self):
        # Switch to sleep mode
        self._set_mode1_register(register_value=RegisterMode1.SLEEP.value)

        # Set pre-scale
        self._set_pre_scale(frequency=self._frequency)

        # Enable oscillator and auto increment
        self._set_mode1_register(register_value=RegisterMode1.AI.value | RegisterMode1.ALLCALL.value)

        # Set all triggers for all ports to 0
        self._set_all_port_triggers(on_trigger=0, off_trigger=0)

        # Save changed state
        self.initialized = True

    def _stop(self):
        # Switching to sleep mode
        self._set_mode1_register(register_value=RegisterMode1.SLEEP.value)
        self.initialized = False

    @property
    def _ready(self) -> bool:
        return bool(self.initialized)

    def _set_all_port_triggers(self, on_trigger, off_trigger):
        data = []
        for trigger_value in (on_trigger, off_trigger,):
            trigger_bytes = pack("<H", trigger_value)
            data.extend(trigger_bytes)
        self.i2c_connection.write_i2c_block_data(
            i2c_addr=self._address,
            register=self.ALL_PORTS_START_REGISTER,
            data=data,
        )

    def _set_mode1_register(self, register_value):
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.MODE1.value,
            value=register_value,
        )

    def _set_port_triggers(self, port, on_trigger, off_trigger):
        start_register = self.PORT_START_REGISTER + self.PORT_REGISTERS_AMOUNT * port

        data = []
        for trigger_value in (on_trigger, off_trigger,):
            trigger_bytes = pack("<H", trigger_value)
            data.extend(trigger_bytes)

        self.i2c_connection.write_i2c_block_data(
            i2c_addr=self._address,
            register=start_register,
            data=data,
        )

    def _set_pre_scale(self, frequency):
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.PRE_SCALE.value,
            value=int(self._osc_clock / (4096 * frequency) - 1),
        )

    def set_port_triggers(self, port, on_us):
        if on_us > self._period_time:
            logger.error("On time is greater than period time: on time '{on}' us, period '{period}' us.".format(
                on=on_us,
                period=self._period_time
            ))
            on_us = self._period_time
        self._set_port_triggers(
            port=port,
            on_trigger=0,
            off_trigger=int(self._ticks_per_us * on_us),
        )
