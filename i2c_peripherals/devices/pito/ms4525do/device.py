import logging
from typing import Union

from i2c_peripherals.constants import Conversions
from i2c_peripherals.mixins.i2c_mixin import I2CMixin
from i2c_peripherals.base.base_peripheral import BasePeripheral
from i2c_peripherals.devices.pito.definitions import Pito

logger = logging.getLogger(__name__)


class MS4525DO(I2CMixin, BasePeripheral):
    TEMPERATURE_START = -50
    TEMPERATURE_STEP = 200 / 2047

    def __init__(self, *args, range_psi, pressure_error=0, **kwargs):
        super().__init__(*args, **kwargs)
        self._pressure_error = pressure_error
        self._range = range_psi * Conversions.PSI
        self.initialized = True

    def _start(self):
        pass

    def _stop(self):
        pass

    def collect_data(self) -> Union[Pito, None]:
        result = None
        if self.initialized:
            pH, pL, tH, tL = self.i2c_connection.read_i2c_block_data(
                i2c_addr=self._address,
                register=0x00,
                length=4,
            )
            pressure_raw_value = ((pH & 0b00111111) << 8) | pL
            temperature_raw_value = (tH << 3) | ((pL & 0b11100000) >> 5)

            dynamic_pressure = 2 * self._range * (pressure_raw_value - 0.1 * 16383) / (0.8 * 16383) - self._range
            temperature = self.TEMPERATURE_START + self.TEMPERATURE_STEP * temperature_raw_value

            result = Pito(
                dynamic_pressure=dynamic_pressure + self._pressure_error,
                temperature=temperature,
            )

        return result
