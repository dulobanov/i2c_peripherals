from dataclasses import dataclass, field
from logging import getLogger
from time import time

from i2c_peripherals.utils.dataclass import FlattenMixin, DictInit

logger = getLogger(__name__)


@dataclass(frozen=True)
class Pito(FlattenMixin, metaclass=DictInit):
    dynamic_pressure: float
    temperature: float
    timestamp: float = field(default_factory=time)
