import logging
import numpy
from collections import namedtuple
from fractions import Fraction
from struct import unpack
from typing import Union

from i2c_peripherals.constants import Constants
from i2c_peripherals.mixins.i2c_mixin import I2CMixin
from i2c_peripherals.base.base_peripheral import BasePeripheral
from i2c_peripherals.devices.barometer.bmp280.configurations import (
    OSRSTemperature, OSRSPressure, PowerMode, StandByTime, Filter, SPI
)
from i2c_peripherals.devices.barometer.definitions import Barometer
from i2c_peripherals.base.exception import InitializationException

logger = logging.getLogger(__name__)

TrimmingParameters = namedtuple(
    "TrimmingParameters",
    ("t1", "t2", "t3", "p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8", "p9",)
)


class BMP280(I2CMixin, BasePeripheral):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._f4 = OSRSTemperature.X16.value | OSRSPressure.X16.value | PowerMode.SCHEDULED.value
        self._trimming_parameters = None

    @property
    def trimming_parameters(self) -> TrimmingParameters:
        if not self._trimming_parameters:
            data = self.i2c_connection.read_i2c_block_data(
                i2c_addr=self._address,
                register=0x88,
                length=24,
            )
            self._trimming_parameters = TrimmingParameters(*unpack("HhhHhhhhhhhh", bytes(data)))

        return self._trimming_parameters

    def __reset(self):
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=0xe0,
            value=0xb6,
        )

    def _start(self):
        self.__reset()
        # TODO: replace with values from init configuration
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=0xf5,
            value=StandByTime.T05.value | Filter.X16.value | SPI.DISABLE.value,
        )
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=0xf4,
            value=self._f4,
        )
        self.initialized = True

    def _stop(self):
        self.__reset()
        self._trimming_parameters = None

    def collect_data(self) -> Union[Barometer, None]:
        if not self.initialized:
            raise InitializationException("Not initialized device, wait for initialization.")
        data = self.i2c_connection.read_i2c_block_data(
            i2c_addr=self._address,
            register=0xf3,
            length=10,
        )
        return self._process_data(*data)

    def _bytes_to_fraction(self, *args):
        result = Fraction(0)
        for i in args:
            result *= 256
            result += i
        return result

    def _calculate_QNH(self, temperature, pressure):
        temperature_kelvin = temperature + Constants.TEMPERATURE_0_KELVIN
        temperature_mean_kelvin = numpy.mean([temperature_kelvin, Constants.QNH_TEMPERATURE_K])
        qnh = Constants.QNH_R * temperature_mean_kelvin * numpy.log((Constants.QNH_PRESSURE_0 / pressure)) / Constants.G
        return qnh

    def _process_data(self, *args) -> Union[Barometer, None]:
        status, f4, *config, p1, p2, p3, t1, t2, t3 = args
        trimming_parameters = self.trimming_parameters
        if status & 0x01:
            logger.warning("BMP280 data is updating, get new sample.")
            return None
        if f4 != self._f4:
            logger.warning("BMP280 lost configuration, resetting.")
            self.stop(force=True)
            self.start()
            return None
        if (p1, p2) == (0x80, 0x00) or (t1, t2) == (0x80, 0x00):
            logger.warning("BMP280 have not get first sample yet, waiting.")
            return None

        rp = self._bytes_to_fraction(p1, p2, p3) / 16  # remove last 4 bits
        rt = self._bytes_to_fraction(t1, t2, t3) / 16  # remove last 4 bits

        v1 = (Fraction(rt, 16384) - Fraction(trimming_parameters.t1, 1024)) * trimming_parameters.t2
        v2 = ((Fraction(rt, 131072) - Fraction(trimming_parameters.t1, 8192)) ** 2) * trimming_parameters.t3
        tfine = v1 + v2

        temperature = tfine / 5120

        v3 = tfine / 2 - 64000
        v4 = (v3 ** 2) * Fraction(trimming_parameters.p6, 32768)
        v5 = v4 + v3 * trimming_parameters.p5 * 2
        v6 = v5 / 4 + trimming_parameters.p4 * 65536
        v7 = (Fraction(trimming_parameters.p3) * (v3 ** 2) / 524288 + trimming_parameters.p2 * v3) / 524288
        v8 = (1 + v7 / 32768) * trimming_parameters.p1

        vp1 = Fraction(1048576 - rp)
        vp2 = (vp1 - v6 / 4096) * 6250 / v8

        v9 = Fraction(trimming_parameters.p9) * (vp2 ** 2) / 2147483648
        v10 = vp2 * Fraction(trimming_parameters.p8, 32768)

        pressure = vp2 + (v9 + v10 + trimming_parameters.p7) / 16

        return Barometer(
            temperature=float(temperature),
            air_density=float(pressure) * Constants.MOLAR_MASS_AIR / (
                    Constants.UNIVERSAL_GAS_CONSTANT * (Constants.TEMPERATURE_0_KELVIN + float(temperature))
            ) / 1000,  # kg/m^3
            pressure=float(pressure),
            QNH=float(self._calculate_QNH(
                temperature=float(temperature),
                pressure=float(pressure),
            )),
        )
