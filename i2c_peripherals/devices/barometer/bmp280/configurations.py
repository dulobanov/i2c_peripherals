from enum import Enum


class OSRSTemperature(Enum):
    X16 = 0b11100000


class OSRSPressure(Enum):
    X16 = 0b00011100


class PowerMode(Enum):
    SLEEP = 0b00000000
    SINGLE = 0b00000010
    SCHEDULED = 0b00000011


class StandByTime(Enum):
    T05 = 0b00000000


class Filter(Enum):
    X16 = 0b00011100


class SPI(Enum):
    DISABLE = 0b00000000
