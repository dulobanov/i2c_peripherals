import logging
from struct import unpack
from time import sleep, time
from typing import Union

from i2c_peripherals.mixins.i2c_mixin import I2CMixin
from i2c_peripherals.base.base_peripheral import BasePeripheral
from i2c_peripherals.base.exception import LockException
from i2c_peripherals.base.state import State
from i2c_peripherals.devices.voltmeter.ads1115.configurations import (
    OperationStatus, PIN, Gain, ConversionMode, SPS, ComparatorMode, ComparatorPolarity, ComparatorLatching,
    ComparatorQueueAndDisable
)
from i2c_peripherals.devices.voltmeter.definitions import VoltmeterMeasurementResult, Voltmeter
from i2c_peripherals.devices.voltmeter.ads1115.exception import (
    BaseI2CDeviceException, MeasurementTimeoutException, MeasurementSkippedException,
)

logger = logging.getLogger(__name__)


class VoltmeterMeasurement:
    def __init__(self, name: str, pin: str, max_voltage: float, interval: float, converter=None):
        self._measurement_name = name
        self._pin = PIN[pin]
        self._max_voltage = max_voltage
        self._measure_interval = interval
        self._converter = converter

    def measure(self, peripheral):
        result = peripheral.get_voltage(
            pin=self._pin,
            max_voltage=self._max_voltage,
        )
        if self._converter:
            result = self._converter(result)
        return float(result)

    @property
    def next_measurement_time(self):
        return time() + self._measure_interval

    @property
    def name(self):
        return self._measurement_name


class ADS1115(I2CMixin, BasePeripheral):
    REGISTER_CONFIGURATION = 0x01
    REGISTER_CONVERSION = 0x00

    last_measurement_time = State()

    def __init__(self, *args, measurements: dict, sps=SPS.SPS_128, **kwargs):
        super().__init__(*args, **kwargs)

        self._measurements = {
            name: VoltmeterMeasurement(name=name, **params) for name, params in measurements.items()
        }

        if not self.last_measurement_time:
            self.last_measurement_time = {mes_name: time() for mes_name in self._measurements.keys()}

        self._sps = SPS(sps)
        self._current_gain = None

    def _reset(self):
        default = (OperationStatus.START_CONVERSION, PIN.AIN0_to_AIN1, Gain.V2_048, ConversionMode.SINGLE, SPS.SPS_128,
                   ComparatorMode.THRESHOLD, ComparatorPolarity.ACTIVE_LOW, ComparatorLatching.NON_LATCHING,
                   ComparatorQueueAndDisable.DISABLE,)
        value = 0x00
        for parameter in default:
            value |= parameter.value
        self.i2c_connection.write_i2c_block_data(
            i2c_addr=self._address,
            register=self.REGISTER_CONFIGURATION,
            data=value.to_bytes(length=2, byteorder="big"),
        )

    @property
    def _ready(self) -> bool:
        return True

    def _start(self):
        pass

    def _stop(self):
        self._reset()

    def _start_measurement(self, pin: PIN, max_voltage: float) -> bool:
        started = False
        self._current_gain = None

        for gain in sorted(list(Gain), key=lambda i: i.max_voltage):
            if gain.max_voltage >= max_voltage:
                self._current_gain = gain
                break

        if self._current_gain:
            default = (OperationStatus.START_CONVERSION, pin, self._current_gain, ConversionMode.SINGLE, self._sps,
                       ComparatorMode.THRESHOLD, ComparatorPolarity.ACTIVE_LOW, ComparatorLatching.NON_LATCHING,
                       ComparatorQueueAndDisable.DISABLE,)
            value = 0x00
            for parameter in default:
                value |= parameter.value
            self.i2c_connection.write_i2c_block_data(
                i2c_addr=self._address,
                register=self.REGISTER_CONFIGURATION,
                data=value.to_bytes(length=2, byteorder="big"),
            )
            started = True
        else:
            logger.warning("Too high max voltage %s, measurement was not started.", max_voltage)
        return started

    def _get_measurement_value(self):
        raw_value = self.i2c_connection.read_i2c_block_data(
            i2c_addr=self._address,
            register=self.REGISTER_CONVERSION,
            length=2,
        )
        raw_value = unpack(">h", bytes(raw_value))[0]
        return self._current_gain.convert(raw_value=raw_value)

    def _is_measurement_ready(self) -> bool:
        raw_value = self.i2c_connection.read_i2c_block_data(
            i2c_addr=self._address,
            register=self.REGISTER_CONFIGURATION,
            length=2,
        )
        raw_value = int.from_bytes(bytes=raw_value, byteorder="big")
        return bool(raw_value & 0x8000)

    def get_voltage(self, pin: PIN, max_voltage: float):
        try:
            with self.state_handler:
                if self._start_measurement(pin=pin, max_voltage=max_voltage):
                    for i in range(5):
                        if self._is_measurement_ready():
                            break
                        sleep(0.01)
                    else:
                        raise MeasurementTimeoutException("Measurement result is not ready in 50ms")
                    return self._get_measurement_value()
                else:
                    raise MeasurementSkippedException()
        except LockException:
            logger.warning("Unable to lock device, measurement skipped for '%s'.", pin)
            raise
        finally:
            self._current_gain = None

    def collect_data(self) -> Union[Voltmeter, None]:
        measurements = []
        measurement_time = self.last_measurement_time

        for measurement in self._measurements.values():
            if time() >= measurement_time[measurement.name]:
                try:
                    measurements.append(
                        VoltmeterMeasurementResult(
                            name=measurement.name,
                            value=measurement.measure(peripheral=self),
                        )
                    )
                    measurement_time[measurement.name] = measurement.next_measurement_time
                except BaseI2CDeviceException:
                    pass
        self.last_measurement_time = measurement_time

        result = None
        if measurements:
            result = Voltmeter(measurements=measurements)

        return result
