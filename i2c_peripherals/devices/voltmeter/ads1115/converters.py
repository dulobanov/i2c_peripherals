def battery_converter(r1, r2, cells=1):
    """
    Connection scheme
    ------- Ucc
        R1
        --- U
        R2
    ------- GND

    :param r1: resistance of R1 resistor
    :param r2: resistance of R2 resistor
    :param cells: amount of battery cells
    :return: voltage per cell
    """
    return lambda u: u * (r1 + r2) / r2 / cells
