from i2c_peripherals.devices.voltmeter.ads1115.device import ADS1115
from i2c_peripherals.devices.voltmeter.ads1115.configurations import (
    OperationStatus, PIN, Gain, ConversionMode, SPS, ComparatorMode, ComparatorPolarity, ComparatorLatching,
    ComparatorQueueAndDisable,
)
