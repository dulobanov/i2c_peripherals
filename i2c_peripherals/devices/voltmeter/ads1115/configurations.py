from enum import Enum


class OperationStatus(Enum):
    NOTHING = 0x0000
    START_CONVERSION = 0x8000


class PIN(Enum):
    AIN0_to_AIN1 = 0x0000
    AIN0_to_AIN3 = 0x1000
    AIN1_to_AIN3 = 0x2000
    AIN2_to_AIN3 = 0x3000
    AIN0_to_GND = 0x4000
    AIN1_to_GND = 0x5000
    AIN2_to_GND = 0x6000
    AIN3_to_GND = 0x7000


class Gain(Enum):
    V6_144 = (0x0000, 6.144,)
    V4_096 = (0x0200, 4.096,)
    V2_048 = (0x0400, 2.048,)
    V1_024 = (0x0600, 1.024,)
    V0_512 = (0x0800, 0.512,)
    V0_256 = (0x0A00, 0.256,)

    def __init__(self, mask, voltage):
        self._value_ = mask
        self._voltage = voltage

    def convert(self, raw_value):
        return self._voltage * raw_value / 2 ** 15

    @property
    def max_voltage(self):
        return self._voltage


class ConversionMode(Enum):
    CONTINUOUS = 0x0000
    SINGLE = 0x0100


class SPS(Enum):
    SPS_8 = 0x0000
    SPS_16 = 0x0020
    SPS_32 = 0x0040
    SPS_64 = 0x0060
    SPS_128 = 0x0080
    SPS_250 = 0x00A0
    SPS_475 = 0x00C0
    SPS_860 = 0x00E0


class ComparatorMode(Enum):
    THRESHOLD = 0x0000
    WINDOW = 0x0010


class ComparatorPolarity(Enum):
    ACTIVE_LOW = 0x0000
    ACTIVE_HIGH = 0x0008


class ComparatorLatching(Enum):
    NON_LATCHING = 0x0000
    LATCHING = 0x0004


class ComparatorQueueAndDisable(Enum):
    QUEUE_1 = 0x0000
    QUEUE_2 = 0x0001
    QUEUE_4 = 0x0002
    DISABLE = 0x0003
