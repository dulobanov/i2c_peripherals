class BaseI2CDeviceException(Exception):
    pass


class MeasurementTimeoutException(BaseI2CDeviceException):
    pass


class MeasurementSkippedException(BaseI2CDeviceException):
    pass
