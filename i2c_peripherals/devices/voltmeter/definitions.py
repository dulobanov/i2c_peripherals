from dataclasses import dataclass, field
from logging import getLogger
from time import time

from i2c_peripherals.utils.dataclass import FlattenMixin, DictInit

logger = getLogger(__name__)


@dataclass(frozen=True)
class VoltmeterMeasurementResult(FlattenMixin, metaclass=DictInit):
    name: str
    value: float


@dataclass(frozen=True)
class Voltmeter(FlattenMixin, metaclass=DictInit):
    measurements: list[VoltmeterMeasurementResult]
    timestamp: float = field(default_factory=time)

    @property
    def flatten(self) -> dict:
        result = {"timestamp": self.timestamp}
        for measurement in self.measurements:
            result[measurement.name] = measurement.value
        return result
