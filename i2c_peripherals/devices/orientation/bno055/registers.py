from dataclasses import dataclass
from enum import Enum


class BaseBNO055Exception(Exception):
    pass


class ConfigurationException(BaseBNO055Exception):
    pass


class Registers(Enum):
    CHIP_ID = 0x00  # Page any
    PAGE_ID = 0x07  # Page any
    ACC_CONFIG = 0x08  # Page 1
    ACCELERATION_DATA_START = 0x08  # Page 0
    MAGNETOMETER_DATA_START = 0x0e  # Page 0
    GYROSCOPE_DATA_START = 0x14  # Page 0
    EULER_START = 0x1A  # Page 0
    QUATERNION_START = 0x20  # Page 0
    LINEAR_ACCELERATION_START = 0x28  # Page 0
    GRAVITY_VECTOR_START = 0x2E  # Page 0
    ST_RESULT = 0x36  # Page 0
    UNIT_SELECTION = 0x3b  # Page 0
    OPR_MODE = 0x3d  # Page 0
    AXIS_MAP_CONFIG = 0x41  # Page 0
    AXIS_MAP_SIGN = 0x42  # Page 0


class Page(Enum):
    PAGE_0 = 0x00
    PAGE_1 = 0x01


class OPR_MODE(Enum):
    CONFIG = 0b00000000
    NDOF = 0b00001100


class AccRange(Enum):
    G2 = 0b00000000
    G4 = 0b00000001
    G8 = 0b00000010
    G16 = 0b00000011


class AccUnit(Enum):
    m_per_s2 = (0b00000000, 100)
    mG = (0b00000001, 1)

    def __init__(self, mask, ratio):
        self._value_ = mask
        self._ratio = ratio

    def convert(self, raw_value: int) -> float:
        return raw_value / self._ratio


class GyroscopeUnit(Enum):
    DPS = (0b00000000, 16)
    RPS = (0b00000010, 900)

    def __init__(self, mask, ratio):
        self._value_ = mask
        self._ratio = ratio

    def convert(self, raw_value: int) -> float:
        return raw_value / self._ratio


class EulerUnit(Enum):
    DEGREES = (0b00000000, 16)
    RADIANS = (0b00000100, 900)

    def __init__(self, mask, ratio):
        self._value_ = mask
        self._ratio = ratio

    def convert(self, raw_value: int) -> float:
        return raw_value / self._ratio


class TemperatureUnit(Enum):
    DEGREES = 0b00000000
    FAHRENHEIT = 0b00010000


class OrientationDataOutputFormat(Enum):
    ANDROID = 0b10000000
    WINDOWS = 0b00000000


class Axis(Enum):
    X = 0b00
    Y = 0b01
    Z = 0b10


# 0x41 & 0x42
@dataclass(frozen=True)
class AxisMap:
    bank: Axis
    pitch: Axis
    yaw: Axis
    change_bank_direction: bool
    change_pitch_direction: bool
    change_yaw_direction: bool

    def __post_init__(self):
        axises = (self.bank, self.pitch, self.yaw,)
        errors = []
        if Axis.X not in axises:
            errors.append("X")
        if Axis.Y not in axises:
            errors.append("Y")
        if Axis.Z not in axises:
            errors.append("Z")
        if errors:
            raise ConfigurationException("Axis(es) '{}' are not defined in remap config.", ", ".join(errors))

    @property
    def map_register(self):
        result = 0b00000000
        result |= self.yaw.value
        result <<= 2
        result |= self.bank.value
        result <<= 2
        result |= self.pitch.value
        return result

    @property
    def direction_register(self):
        directions = {
            axis: direction for axis, direction in (
                (self.bank, self.change_bank_direction,),
                (self.pitch, self.change_pitch_direction),
                (self.yaw, self.change_yaw_direction),
            )
        }

        result = 0b00000000
        for axis in (Axis.X, Axis.Y, Axis.Z,):
            result |= int(directions[axis])
            result <<= 1
        result >>= 1
        return result
