import logging
from numpy import sqrt
from struct import unpack
from typing import Union

from i2c_peripherals.mixins.device_ready import DeviceReadyMixin
from i2c_peripherals.mixins.i2c_mixin import I2CMixin
from i2c_peripherals.base.base_peripheral import BasePeripheral
from i2c_peripherals.devices.orientation.definitions import Vector, Quaternion, AxisParameters, DOF9Data
from i2c_peripherals.devices.orientation.bno055.registers import (
    BaseBNO055Exception, Registers, OPR_MODE, AccRange, AccUnit, GyroscopeUnit, EulerUnit, TemperatureUnit,
    OrientationDataOutputFormat, Page, Axis, AxisMap,
)

logger = logging.getLogger(__name__)


class BNO055InitializationException(BaseBNO055Exception):
    pass


class BNO055(I2CMixin, DeviceReadyMixin, BasePeripheral):
    CHIP_ID = 0xa0

    QUATERNION = 2 ** 14
    MAGNETOMETER_uT = 16  # 1uT = 16 LSB

    def __init__(
            self,
            *args,
            acc_range="G4",
            pitch_axis=Axis.X.name,
            bank_axis=Axis.Y.name,
            yaw_axis=Axis.Z.name,
            change_bank_direction: bool = False,
            change_pitch_direction: bool = False,
            change_yaw_direction: bool = False,
            **kwargs,
    ):
        self._acc_range = AccRange[acc_range]
        self._axis_map = AxisMap(
            bank=Axis[bank_axis],
            pitch=Axis[pitch_axis],
            yaw=Axis[yaw_axis],
            change_bank_direction=change_bank_direction,
            change_pitch_direction=change_pitch_direction,
            change_yaw_direction=change_yaw_direction,
        )
        self._acc_unit = AccUnit.m_per_s2
        self._gyr_unit = GyroscopeUnit.DPS
        super().__init__(*args, **kwargs)

    def _start(self):
        # Set Page to 0
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.PAGE_ID.value,
            value=Page.PAGE_0.value,
        )
        # Check device ID
        chip_id, acc_id, mag_id, gyr_id, sw_l, sw_h, bl_rev_id, page_id = self.i2c_connection.read_i2c_block_data(
            i2c_addr=self._address,
            register=Registers.CHIP_ID.value,
            length=8,
        )
        if chip_id != self.CHIP_ID:
            raise BNO055InitializationException("Expected Chip id is '{exp}', got '{got}'.".format(
                exp=self.CHIP_ID,
                got=chip_id,
            ))

        logger.info("Accelerometer ID is '%s'.", acc_id)
        logger.info("Magnetometer ID is '%s'.", mag_id)
        logger.info("Gyroscope ID is '%s'.", gyr_id)
        logger.info("Software version ID is '%s.%s'.", sw_h, sw_l)
        logger.info("Bootloader revision ID is '%s'.", bl_rev_id)

        # Check SelfTest status
        status = self.i2c_connection.read_byte_data(
            i2c_addr=self._address,
            register=Registers.ST_RESULT.value,
        )
        if status != 0x0f:
            raise BNO055InitializationException("Self test status displays problems in chip '{}'.".format(status))

        # Configure Units
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.UNIT_SELECTION.value,
            value=(
                    self._acc_unit.value | self._gyr_unit.value | EulerUnit.DEGREES.value |
                    TemperatureUnit.DEGREES.value | OrientationDataOutputFormat.ANDROID.value
            ),
        )

        # set accelerometer range
        self._set_acc_settings()

        # Configure Axis map
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.AXIS_MAP_CONFIG.value,
            value=self._axis_map.map_register,
        )
        # Configure Axis direction
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.AXIS_MAP_SIGN.value,
            value=self._axis_map.direction_register,
        )

        # Switch mode
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.OPR_MODE.value,
            value=OPR_MODE.NDOF.value,
        )
        self.initialized = True

    def _stop(self):
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.OPR_MODE.value,
            value=OPR_MODE.CONFIG.value,
        )
        self.initialized = False

    def _device_ready(self) -> bool:
        # Check device status
        status = self.i2c_connection.read_byte_data(
            i2c_addr=self._address,
            register=Registers.OPR_MODE.value,
        )
        return status == OPR_MODE.NDOF.value

    def _set_acc_settings(self):
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.PAGE_ID.value,
            value=Page.PAGE_1.value,
        )
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.ACC_CONFIG.value,
            value=self._acc_range.value,
        )
        self.i2c_connection.write_byte_data(
            i2c_addr=self._address,
            register=Registers.PAGE_ID.value,
            value=Page.PAGE_0.value,
        )

    def _to_vector(self, data: list, func) -> Vector:
        values = unpack("<hhh", bytes(data))
        x, y, z = list(map(func, values))
        return Vector(x=x, y=y, z=z, length=sqrt(x ** 2 + y ** 2 + z ** 2))

    def _to_axis_parameters(self, data: list, func) -> AxisParameters:
        values = unpack("<hhh", bytes(data))
        x, y, z = list(map(func, values))
        return AxisParameters(x=x, y=y, z=z)

    def _euler_orientation(self, data) -> AxisParameters:
        values = unpack("<hhh", bytes(data))
        z, y, x = list(map(self._gyr_unit.convert, values))
        return AxisParameters(x=x, y=y, z=z)

    def _quaternion_orientation(self, data) -> Quaternion:
        values = unpack("<hhhh", bytes(data))
        w, x, y, z = list(map(lambda raw_value: raw_value / self.QUATERNION, values))
        return Quaternion(w=w, x=x, y=y, z=z)

    def collect_data(self) -> Union[None, DOF9Data]:
        result = None
        if self.device_ready:
            immediate_parameters = self.i2c_connection.read_i2c_block_data(
                i2c_addr=self._address,
                register=Registers.ACCELERATION_DATA_START.value,
                length=18,  # ACCELERATION 6 + MAGNETOMETER 6 + GYROSCOPE 6
            )
            preprocessed_parameters = self.i2c_connection.read_i2c_block_data(
                i2c_addr=self._address,
                register=Registers.EULER_START.value,
                length=26,  # EULER 6 + QUATERNION 8 + LINEAR ACC 6 + GRV VECTOR 6
            )

            # immediate parameters
            acceleration = self._to_vector(data=immediate_parameters[0:6], func=self._acc_unit.convert)
            magnetometer = self._to_vector(data=immediate_parameters[6:12], func=lambda rv: rv / self.MAGNETOMETER_uT)
            gyroscope = self._to_axis_parameters(data=immediate_parameters[12:18], func=self._gyr_unit.convert)
            logger.debug("Acceleration: %s m/s**2", acceleration)
            logger.debug("Magnetometer: %s uT", magnetometer)
            logger.debug("Gyroscope: %s dps", gyroscope)

            # preprocessed parameters
            orientation_euler = self._euler_orientation(data=preprocessed_parameters[0:6])
            orientation_quaternion = self._quaternion_orientation(data=preprocessed_parameters[6:14])
            linear_acceleration = self._to_vector(
                data=preprocessed_parameters[14:20],
                func=self._acc_unit.convert,
            )
            gravity_vector = self._to_vector(data=preprocessed_parameters[20:], func=self._acc_unit.convert)
            logger.debug("Linear acceleration: %s m/s**2", linear_acceleration)
            logger.debug("Gravity vector: %s", gravity_vector)
            logger.debug("Euler: %s", orientation_euler)
            logger.debug("Quaternion: %s", orientation_quaternion)
            result = DOF9Data(
                acceleration=acceleration,
                magnetometer=magnetometer,
                gyroscope=gyroscope,
                linear_acceleration=linear_acceleration,
                gravity=gravity_vector,
                euler=orientation_euler,
                quaternion=orientation_quaternion,
            )
        else:
            logger.warning("BNO055 is not initialized.")
            self.start()
        return result
