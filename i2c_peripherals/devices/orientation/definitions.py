from dataclasses import dataclass
from logging import getLogger

from i2c_peripherals.utils.dataclass import FlattenMixin, DictInit

logger = getLogger(__name__)


@dataclass(frozen=True)
class Vector(FlattenMixin, metaclass=DictInit):
    x: float
    y: float
    z: float
    length: float


@dataclass(frozen=True)
class Quaternion(FlattenMixin, metaclass=DictInit):
    w: float
    x: float
    y: float
    z: float


@dataclass(frozen=True)
class AxisParameters(FlattenMixin, metaclass=DictInit):
    x: float
    y: float
    z: float


@dataclass(frozen=True)
class DOF9Data:
    acceleration: Vector
    magnetometer: Vector
    gyroscope: AxisParameters

    linear_acceleration: Vector
    gravity: Vector
    euler: AxisParameters
    quaternion: Quaternion
