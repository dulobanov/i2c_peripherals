from abc import ABC, abstractmethod

from i2c_peripherals.mixins.is_ready import IsReadyMixin


class StartStopInterface(IsReadyMixin, ABC):

    @abstractmethod
    def _start(self):
        pass

    def start(self, *, force=False):
        if not self.is_ready() or force:
            self._start()

    @abstractmethod
    def _stop(self):
        pass

    def stop(self, *, force=False):
        if self.is_ready() or force:
            self._stop()
