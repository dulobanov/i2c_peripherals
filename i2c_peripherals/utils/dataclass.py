from logging import getLogger

logger = getLogger(__name__)


class FlattenException(Exception):
    pass


class DictInit(type):
    def __call__(cls, *args, **kwargs):
        if not kwargs and len(args) == 1 and isinstance(args[0], dict):
            kwargs = args[0]
            args = tuple()

        initial_kwargs = kwargs
        kwargs = {}
        for name, value in initial_kwargs.items():
            expected_type = cls.__annotations__.get(name)
            if not expected_type:
                continue
            if type(value) != expected_type:
                try:
                    value = expected_type(value)
                except Exception as exc:
                    logger.error("Unable to convert '%s' field to '%s' type, value '%s', %s",
                                 name, expected_type, value, exc)
            kwargs[name] = value
        return super(DictInit, cls).__call__(*args, **kwargs)


class AsDictMixin:
    def as_dict(self) -> dict:
        result = {}
        for name, value in self.__dict__.items():
            if isinstance(value, AsDictMixin):
                value = value.as_dict()
            elif isinstance(value, list):
                value = [i.as_dict() if isinstance(i, AsDictMixin) else i for i in value]
            result[name] = value
        return result


def get_flatten(data: dict, parent_name=None, separator="__") -> dict:
    result = {}
    for key, value in data.items():
        if value is None:
            continue
        if isinstance(value, AsDictMixin):
            value = value.as_dict()
        complete_key = separator.join([parent_name, key]) if parent_name else key
        if isinstance(value, dict):
            result.update(get_flatten(
                data=value,
                parent_name=complete_key,
            ))
        elif isinstance(value, (str, int, float)):
            result[complete_key] = value
        else:
            raise FlattenException("Unable to convert '{}' value type to flat one.".format(type(value)))
    return result


class FlattenMixin(AsDictMixin):

    @property
    def flatten(self) -> dict:
        return get_flatten(data=self.__dict__)
