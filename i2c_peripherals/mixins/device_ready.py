from abc import ABC, abstractmethod
from logging import getLogger
from time import time

from i2c_peripherals.mixins.is_ready import IsReadyMixin

logger = getLogger(__name__)


class DeviceReadyMixin(IsReadyMixin, ABC):
    DEVICE_CHECK_READY_TIMEOUT = 1

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__device_ready_check_timestamp = time() + self.DEVICE_CHECK_READY_TIMEOUT

    @abstractmethod
    def _device_ready(self) -> bool:
        pass

    @property
    def device_ready(self) -> bool:
        initialized = self.initialized
        if initialized and time() > self.__device_ready_check_timestamp:
            initialized = self._device_ready()
            if not initialized:  # update device initialized status if it different from True
                logger.warning("Device '%s' has lost it's configuration.", self.name)
                self.initialized = initialized
            self.__device_ready_check_timestamp = time() + self.DEVICE_CHECK_READY_TIMEOUT
        return initialized
