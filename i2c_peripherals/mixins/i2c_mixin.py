import logging

from smbus2 import SMBus

logger = logging.getLogger(__name__)


class I2CMixin:

    def __init__(self, i2c_bus, address, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._i2c_bus = i2c_bus
        self._i2c_connection = None
        self._address = address

    @property
    def i2c_connection(self):
        if not self._i2c_connection:
            self._i2c_connection = SMBus(bus=self._i2c_bus)
        return self._i2c_connection
