from abc import ABC
from time import time, sleep

from i2c_peripherals.mixins.name import NameMixin
from i2c_peripherals.base.state import State


class IsReadyMixin(NameMixin, ABC):
    READY_DELAY = 0.005
    NOT_READY_EXCEPTION = None

    initialized = State(default_value=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert self.NOT_READY_EXCEPTION, "Please define NOT_READY_EXCEPTION exception."

    @property
    def _ready(self) -> bool:
        return bool(self.initialized)

    def is_ready(self, *, wait: bool = False, timeout=None, raise_on_wait=True) -> bool:
        if not self._ready and wait:
            stop_time = None
            if timeout > 0:
                stop_time = time() + timeout
            while not self._ready:
                if stop_time and stop_time < time():
                    break
                sleep(self.READY_DELAY)
            if not self._ready and raise_on_wait:
                raise self.NOT_READY_EXCEPTION("Resource has not became ready in '{}' seconds".format(timeout))
        return self._ready
