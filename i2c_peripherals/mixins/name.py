class NameMixin:

    def __init__(self, name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__name = name

    @property
    def name(self):
        return self.__name
