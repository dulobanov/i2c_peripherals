class BasePeripheralException(Exception):
    pass


class InitializationException(BasePeripheralException):
    pass


class LockException(BasePeripheralException):
    pass
