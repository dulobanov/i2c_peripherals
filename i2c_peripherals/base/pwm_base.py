from abc import ABC

from i2c_peripherals.base.base_peripheral import BasePeripheral


# TODO: it's a base class, not a mixin
class PWMBase(BasePeripheral, ABC):
    def __init__(
            self, pwm, *args,
            min_up_time: int, max_up_time: int, min_control_value: int, max_control_value: int,
            **kwargs
    ):
        super().__init__(*args, **kwargs)
        self._pwm = self._peripheral_factory.get_peripheral(name=pwm)

        assert max_up_time > min_up_time, "Max UP time should be higher than Min UP time."
        self._min_up_time = min_up_time
        self._max_up_time = max_up_time
        self._up_time_delta = max_up_time - min_up_time

        assert max_control_value > min_control_value, "Max UP control value should be higher than Min control value."
        self._min_control_value = min_control_value
        self._max_control_value = max_control_value
        self._control_value_delta = max_control_value - min_control_value

        self._us_per_percent_control_value = (max_up_time - min_up_time) / (max_control_value - min_control_value)

    @property
    def pwm(self):
        return self._pwm

    def _control_value_to_pwm_up_us(self, value) -> int:
        if value < self._min_control_value:
            value = self._min_control_value
        elif value > self._max_control_value:
            value = self._max_control_value
        return int(round(
            self._min_up_time + self._up_time_delta * (value - self._min_control_value) / self._control_value_delta, 0
        ))

    def _pwm_us_to_control_value(self, pwm_up_us) -> float:
        result = (
                self._min_control_value +
                self._control_value_delta * (pwm_up_us - self._min_up_time) / self._up_time_delta
        )
        if result < 0:
            result = -1
        return result
