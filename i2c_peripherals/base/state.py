import json
from abc import ABCMeta, abstractmethod
from threading import Lock as ThreadLock

from redis import ConnectionPool, Redis
from redis_lock import Lock as RedisLock

from i2c_peripherals.base.exception import LockException
from i2c_peripherals.mixins.name import NameMixin


class StateHandlerInterface(metaclass=ABCMeta):
    @abstractmethod
    def set(self, name, value):
        pass

    @abstractmethod
    def get(self, name, default=None):
        pass

    @abstractmethod
    def lock(self, blocking=True, timeout=-1):
        pass

    @abstractmethod
    def release(self):
        pass

    def __enter__(self):
        if not self.lock(blocking=True):
            raise LockException("Unable to acquire log.")
        return self.lock

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()


class DummyStateHandler(StateHandlerInterface):
    def __init__(self):
        self._lock = ThreadLock()
        self._values = {}

    def set(self, name, value):
        self._values[name] = value

    def get(self, name, default=None):
        return self._values.get(key=name, default=default)

    def lock(self, blocking=True, timeout=-1):
        return self._lock.acquire(blocking=blocking, timeout=timeout)

    def release(self):
        self._lock.release()


class RedisStateHandler(StateHandlerInterface):
    def __init__(self, host, port, database, lock_timeout=1, max_connections=2):
        self._redis = Redis(connection_pool=ConnectionPool(
            host=host,
            port=port,
            db=database,
            max_connections=max_connections,
        ))
        self._lock = RedisLock(
            redis_client=self._redis,
            name=self.__class__.__name__,
            expire=lock_timeout,
        )

    def set(self, name, value):
        self._redis.set(name=name, value=value)

    def get(self, name, default=None):
        return self._redis.get(name=name) or default

    def lock(self, blocking=True, timeout=None):
        return self._lock.acquire(blocking=blocking, timeout=timeout)

    def release(self):
        self._lock.release()


class State:
    def __init__(self, default_value=None):
        self._property_name = None
        self._default_value = default_value

    def __set_name__(self, owner, name):
        assert issubclass(owner, NameMixin), "State should be used only with subclasses of NameMixin."
        self._property_name = name

    def __set__(self, instance, value):
        instance.state_handler.set(
            name="{name}__{property_name}".format(
                name=instance.name,
                property_name=self._property_name,
            ),
            value=json.dumps(value),
        )

    def __get__(self, instance, owner):
        result = instance.state_handler.get(
            name="{name}__{property_name}".format(
                name=instance.name,
                property_name=self._property_name,
            ),
        )
        result = self._default_value if result is None else json.loads(result)
        return result
