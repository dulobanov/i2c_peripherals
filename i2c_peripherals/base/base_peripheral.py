from abc import ABC

from i2c_peripherals.base.exception import InitializationException
from i2c_peripherals.base.state import DummyStateHandler
from i2c_peripherals.interfaces.start_stop_interface import StartStopInterface


class BasePeripheral(StartStopInterface, ABC):
    NOT_READY_EXCEPTION = InitializationException

    state_handler = DummyStateHandler()

    def __init__(self, *args, name, peripheral_factory, **kwargs):
        self._peripheral_factory = peripheral_factory
        super().__init__(*args, name=name, **kwargs)
        self._name = name
