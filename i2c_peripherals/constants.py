class Constants:
    G: float = 9.807
    TEMPERATURE_0_KELVIN: float = 273.15  # K

    UNIVERSAL_GAS_CONSTANT: float = 8.31446  # J / mol * K
    MOLAR_MASS_AIR: float = 28.96  # g/mol

    QNH_TEMPERATURE_C: float = 16  # C
    QNH_TEMPERATURE_K: float = QNH_TEMPERATURE_C + TEMPERATURE_0_KELVIN  # K
    QNH_PRESSURE_0: float = 101325  # Pa
    QNH_R: float = 287.05


class Conversions:
    PSI = 6894.7572932  # Pa
