from setuptools import setup, find_packages

setup(
    name="i2c-peripherals",
    version="0.0.1",
    packages=find_packages(),
    install_requires=[
        "numpy",
        "redis",
        "python-redis-lock",
        "smbus2",
    ],
)
